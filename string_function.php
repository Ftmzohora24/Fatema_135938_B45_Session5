<?php

$str ="Is your name O'Reilley?";
echo addslashes($str);
echo"<br>";


// explode
$str = "html, css, php, mysql";
$arr = explode(" ", $str);
echo "<pre>";
print_r($arr);
echo"<pre>";

// trim
$username = " rahim ";
echo $username;

echo"<br><br>";

echo trim($username);
echo"<br><br>";
echo rtrim($username);
echo"<br><br>";
echo ltrim($username);

// new line break
echo"<br><br>";

echo nl2br("one line, \n Another line");

//str_pad (string, length, pad_string, pad_type)
echo"<br>";

$myStr = "Hello World! How is Life! ";

echo str_pad ($myStr, 2, $myStr); "<br>";
echo"<br>";
echo $myStr;
echo str_repeat ($myStr, 2); "<br>";

echo"<br>";

//substr_compare

$mainStr = "Hello World!How is Life";
$str = "Hello World!How is";
echo substr_compare($mainStr,$str,9);

//substr_count

$mainStr = "Hello World!How is Life";
$str = "He";
echo substr_count($mainStr,$str);

echo "<br>";

$mainStr = "Hello World! How is Life? hey you, How are you?";

echo substr_replace($mainStr, "How are you doing?",2 );


echo "<br>";

$mainStr = "hello world! how is life? hey you, how are you?";

echo ucfirst($mainStr);

echo "<br>";

echo ucwords($mainStr);

?>